# nvim

# 介绍

My personal Neovim environments

# Install

support tools:
steam++

## Support Plugin

```shell
Nodejs
packer.nvim
tree-sitter
ctags + vim-gutentag(opt plugin)
```

1. set system environments:

need to install pack
	`luarocks` lua pack management

```shell
-- The config file is located at:
--   Unix        ~/.config/nvim/init.vim (or init.lua)
--   Windows     ~/AppData/Local/nvim/init.vim (or init.lua)
--
-- if $XDG_CONFIG_HOME is defined:
--           $XDG_CONFIG_HOME/nvim/init.vim (or init.lua)
--
-- as same: $XDG_DATA_HOME
--
-- tree-sitter github down window
--
-- prel[option]:
--   cpan install Neovim::Ext moudle
--
-- lsp request:
--   1. node.js
--   		remark
--   2. npm or yarn
--   3. luarocks
--   4. chocolatey -- important
--   		neovim
--   5. python3
--   		pyvim
--
-- telescope request:
--   `brew install ripgrep` or `choco install ripgrep`
--   `brew install fd` or `sudo apt install fd-find` or `choco install fd`
--
-- windows PATH:
-- user PATH:
--   %ChocolateyToolsLocation% = 'D:\envs\chocolatey\tools'
--   %GOPATH% = 'C:\Users\feiya\go'
-- sys PATH:
--   %CHOCO_TOOLS_HOME% = 'D:\envs\chocolatey\tools'
--   %ChocolateyInstall% = 'D:\envs\chocolatey\chocolatey'
--   %NEOVIM% = '%CHOCO_TOOLS_HOME%\neovim\nvim-win64'
--   %XDG_CONFIG_HOME% = 'D:\Neovim'
--   %XDG_DATA_HOME% = 'D:\Neovim'
```

2. packer.nvim

Download the packer.nvim by:

```shell
# https://github.com/wbthomason/packer.nvim
git clone https://github.com/wbthomason/packer.nvim "$env:XDG_DATA_HOME\nvim-data\site\pack\packer\start\packer.nvim"
```
3. tree-sitter

```shell
https://github.com/tree-sitter/tree-sitter
```

## Windows

### require

```shell
Windows Terminal
Chocolatey
```
#### Windows Terminal (opt)

can not run any script error , fix it by Powershell:

```shell
set-ExecutionPolicy RemoteSigned
```

1. Microsoft Store
2. https://github.com/microsoft/terminal

Path:
`"C:\Program Files\WindowsApps\Microsoft.WindowsTerminal_1.12.10982.0_x64__8wekyb3d8bbwe\wt.exe"`

#### Nodejs

Download by:
```shell
http://nodejs.cn/download/
```

#### Chocolatey

> Windows pack Manager

 https://chocolatey.org/install

```shell
# Powershell
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
```
choco install packs:
```shell
# step 1
choco install ctags fd go gsudo gzip julia lazygit neovim php python ripgrep ruby Wget -y
# step 2
choco install --install-arguments="'/DIR=D:\envs\chocolatey\'" -y lua luarocks
# step 3
choco install mingw -y	# nvim-treesitter need it
```

mingw version:
```shell
gcc.exe (MinGW-W64 x86_64-posix-seh, built by Brecht Sanders) 11.2.0
Copyright (C) 2021 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
```

**luarocks**



choco install list, such:
```shell
PS D:\Neovim\nvim> choco list -l
	Chocolatey v1.1.0
	chocolatey 1.1.0
	chocolatey-compatibility.extension 1.0.0
	chocolatey-core.extension 1.4.0
	chocolatey-dotnetfx.extension 1.0.1
	chocolatey-visualstudio.extension 1.10.2
	chocolatey-windowsupdate.extension 1.0.4
ctags 5.8.1
	dotnetfx 4.8.0.20190930
fd 8.3.2
go 1.18.2
gsudo 1.3.0
gzip 1.3.12
julia 1.7.2
	KB2919355 1.0.20160915
	KB2919442 1.0.20160915
	KB2999226 1.0.20181019
	KB3033929 1.0.5
	KB3035131 1.0.3
lazygit 0.34
lua 5.1.5.52
luarocks 2.4.4	# Download by http://luarocks.github.io/luarocks/releases/ and put exe into lua dir.
neovim 0.7.0
openssl 1.1.1.1500
php 8.1.6
python 3.10.4
python3 3.10.4
ripgrep 13.0.0.20210621
ruby 3.1.1.1
ruby.install 3.1.1.1
	vcredist140 14.32.31326
	vcredist2005 8.0.50727.619501
	vcredist2015 14.0.24215.20170201
	visualstudio-installer 2.0.3
	visualstudio2019-workload-vctools 1.0.1
	visualstudio2019buildtools 16.11.15.0
Wget 1.21.3
```

#### Vim-plug

1. install by copy from:
```shell
feyoung/nvim/nvim-data/
```
2. install by internet:
Powershell:
```shell
# origin
iwr -useb https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim |`
    ni "$(@($env:XDG_DATA_HOME, $env:LOCALAPPDATA)[$null -eq $env:XDG_DATA_HOME])/nvim-data/site/autoload/plug.vim" -Force

# my gitee
iwr -useb https://gitee.com/feyoungl/vim-plug/raw/master/plug.vim |`
    ni "$(@($env:XDG_DATA_HOME, $env:LOCALAPPDATA)[$null -eq $env:XDG_DATA_HOME])/nvim-data/site/autoload/plug.vim" -Force
```

#### cmp-tabnine

Power Shell:
```shell
cd nvim-data/site/pack/packer/start/cmp-tabnine/install.ps1
./install.ps1
```

will get binaries/

#### Error

1. Error Infomation:
```shell
Error executing vim.schedule lua callback: Failed to load parser: uv_dlopen: D:\Neovim\nvim-data\site\pack\packer\start\nvim-treesitter\parser\lua.so is not a valid Win32 application.
```

2. github wiki:
```shell
https://github.com/nvim-treesitter/nvim-treesitter/issues/1985
```

3. fix:

```shell
# https://github.com/nvim-treesitter/nvim-treesitter/wiki/Windows-support
#
# install lua-language-server # https://github.com/sumneko/lua-language-server/
#
# system envs:
# %LUA_SERVER_HOME_324% = D:\envs\lua-language-server-3.2.4-win32-x64

choco install mingw

refreshenv
#install parsers in Neovim via:
:TSInstall c
:TSInstall cpp
```

## Mac or Linux

### require

```shell
homebrew or apt
```

#### Vim-plug

1. install by copy from:
```shell
feyoung/nvim/nvim-data/
```

# Doc

## Windows Powershell
`Microsoft.PowerShell_profile.ps1`

This file is locate in windows `Documents/WindowsPowerShell/Microsoft.PowerShell_profile.ps1`, to alias the `vi/vim`, or `lazygit` rename to `lg`.


# 使用说明


# 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

