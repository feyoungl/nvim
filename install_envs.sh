#!/bin/sh

# nvim plug auto install script.

echo "================ begin ================"
echo "Ready to auto install nvim plug."

unames=`uname -s`

if [ "${unames}" == "Darwin" ]; then
    echo "Arch: Mac"
    echo "\nexport XDG_CONFIG_HOME=~/.config\nexport XDG_DATA_HOME=~/.config\nalias vi='nvim'\nalias vim='nvim'" >> ~/.bash_profile
    source ~/.bash_profile
    git clone --depth 1 https://github.com/wbthomason/packer.nvim\
        $XDG_CONFIG_HOME/nvim/site/pack/packer/start/packer.nvim
elif [ "${unames}" == "Linux" ]; then
    echo "Arch: Linux"
    echo "\nexport XDG_CONFIG_HOME=~/.config\nexport XDG_DATA_HOME=~/.config\nalias vi='nvim'\nalias vim='nvim'" >> ~/.bash_profile
    source ~/.bash_profile
    git clone --depth 1 https://github.com/wbthomason/packer.nvim\
        $XDG_CONFIG_HOME/nvim/site/pack/packer/start/packer.nvim
elif [ "${unames}" == "centos" ]; then
    echo "Arch: Centos"
    echo "\nexport XDG_CONFIG_HOME=~/.config\nexport XDG_DATA_HOME=~/.config\nalias vi='nvim'\nalias vim='nvim'" >> ~/.bash_profile
    source ~/.bash_profile
    git clone --depth 1 https://github.com/wbthomason/packer.nvim\
        $XDG_CONFIG_HOME/nvim/site/pack/packer/start/packer.nvim
elif [ "${unames}" == "ubuntu" ]; then
    echo "Arch: Ubuntu"
    echo "\nexport XDG_CONFIG_HOME=~/.config\nexport XDG_DATA_HOME=~/.config\nalias vi='nvim'\nalias vim='nvim'" >> ~/.bash_profile
    source ~/.bash_profile
    git clone --depth 1 https://github.com/wbthomason/packer.nvim\
        $XDG_CONFIG_HOME/nvim/site/pack/packer/start/packer.nvim
else
    echo "Arch: Windows"
    echo ">>> please set envs %XDG_CONFIG_HOME% and %XDG_DATA_HOME% and run:"
    # \\ maybe is \
    echo 'git clone https://github.com/wbthomason/packer.nvim "$env:XDG_DATA_HOME\nvim-data\site\pack\packer\start\packer.nvim"'
	echo 'choro install ctags fd go gsudo gzip julia lazygit lua luarocks neovim php python ripgrep ruby Wget'
fi




echo "================ done ================"

