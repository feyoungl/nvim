-- >>> this file is init lsp config and correlate plugins, likes nvim-cmp and so on.

-- Enable some language servers with the additional completion capabilities offered by nvim-cmp
local _servers 	= {
	{
		name = 'lemminx', -- xml
	},
	{
		name = 'sumneko_lua',
		setting = {
			Lua = {
				runtime = {
					-- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
					version = 'LuaJIT',
				},
				diagnostics = {
					-- Get the language server to recognize the `vim` global
					globals = { 'vim', 'use' },
				},
				workspace = {
					-- Make the server aware of Neovim runtime files
					library = vim.api.nvim_get_runtime_file("", true),
				},
				-- Do not send telemetry data containing a randomized but unique identifier
				telemetry = {
					enable = false,
				},
			},
		},
	},
	{
		name = 'clangd',
		cmd = { 'clangd' },
		filetypes = {
			'c', 'cpp', 'objc', 'objcpp'
		},
		root_pattern = {
			'.clangd',
			'.clang-tidy',
			'.clang-format',
			'compile_commands.json',
			'compile_flags.txt',
			'configure.ac',
			'.git'
		},
	},
	{
		name = 'cmake',
	},
	{
		name = 'vimls',
	},
	{
		name = 'jsonls',
	},
}

-- nvim-lsp-installer
local lsp_installer = require('nvim-lsp-installer')
local lsp_installer_servers = require('nvim-lsp-installer.servers')

lsp_installer.setup({
	automatic_installation = true, -- automatically detect which servers to install (based on which servers are set up via lspconfig)
	ui = {
		icons = {
			server_installed = "✓",
			server_pending = "➜",
			server_uninstalled = "✗"
		}
	}
})


-- Add additional capabilities supported by nvim-cmp
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').update_capabilities(capabilities)

local lspconfig = require('lspconfig')
local _lsp_title = 'Lsp Language Servers'

-- init servers
for _, lsp in ipairs(_servers) do
	local server_available, ser = lsp_installer_servers.get_server(lsp.name)

	if not ser:is_installed() then
		vim.notify('Install language Server: ' .. lsp.name, 'Info', {title = _lsp_title})
		ser:install()
	end

	if not server_available then
		vim.notify('Lsp language Sever: [' .. lsp.name .. '] need to check', 'Warn', {title = _lsp_title})
	end

	lspconfig[lsp.name].setup ({
		-- on_attach = my_custom_on_attach,
		on_attach = Lsp_on_attach, -- 'Lsp_on_attach' defined key-mapping-init.lua
		capabilities = capabilities,
		-- this will be the default in veovim 0.7+
		debounce_text_changes = 150,
		cmd = lsp.cmd,
		root_pattern = lsp.root_pattern,
		filetypes = lsp.filetypes,
		settings = lsp.setting,
	})
end


-- code diagnostic
vim.diagnostic.config({
	virtual_text = {
		-- such as: '●', '▎', 'x' '■' ''
		prefix = "",
		-- 'always' display
		source = "always"
	},
	float = {
		source = "always"
	},
	update_in_insert = false
})

-- ==== cmp ====
local cmp = require('cmp')
local luasnip = require('luasnip')
local lspkind = require('lspkind')
local tabnine = require('cmp_tabnine.config')
local lsp_signature = require('lsp_signature')

local compare = cmp.config.compare

-- "cmp.setup -> symbol_map" Theme
-- local _cmp_symbol_default = {
-- 	Text = "",
-- 	Method = "",
-- 	Function = "",
-- 	Constructor = "",
-- 	Field = "ﰠ",
-- 	Variable = "",
-- 	Class = "ﴯ",
-- 	Interface = "",
-- 	Module = "",
-- 	Property = "ﰠ",
-- 	Unit = "塞",
-- 	Value = "",
-- 	Enum = "",
-- 	Keyword = "",
-- 	Snippet = "",
-- 	Color = "",
-- 	File = "",
-- 	Reference = "",
-- 	Folder = "",
-- 	EnumMember = "",
-- 	Constant = "",
-- 	Struct = "פּ",
-- 	Event = "",
-- 	Operator = "",
-- 	TypeParameter = ""
-- }

local _cmp_symbol_vscode = {
	Text = ' Text',
	Method = ' Met',
	Function = ' Func',
	Constructor = ' Cstr',
	Field = ' Field',
	Variable = ' Vara',
	Class = ' Class',
	Interface = ' Iterf',
	Module = ' Modu',
	Property = ' Prop',
	Unit = ' Unit',
	Value = ' Val',
	Enum = ' Enum',
	Keyword = ' Kword',
	Snippet = ' Snip',
	Color = ' Color',
	File = ' File',
	Reference = ' Refr',
	Folder = ' Folder',
	EnumMember = ' Enumb',
	Constant = ' Cota',
	Struct = ' Struc',
	Event = ' Event',
	Operator = ' Opera',
	TypeParameter = ' Typep',
}

-- symbol map setting
local _cmp_symbol_list = _cmp_symbol_vscode

-- vscode Dark+ Theme Colors to the menu
vim.cmd([[
	" gray
	highlight! CmpItemAbbrDeprecated guibg=NONE gui=strikethrough guifg=#808080
	" blue
	highlight! CmpItemAbbrMatch guibg=NONE guifg=#569CD6
	highlight! CmpItemAbbrMatchFuzzy guibg=NONE guifg=#569CD6
	" light blue
	highlight! CmpItemKindVariable guibg=NONE guifg=#9CDCFE
	highlight! CmpItemKindInterface guibg=NONE guifg=#9CDCFE
	highlight! CmpItemKindText guibg=NONE guifg=#9CDCFE
	" pink
	highlight! CmpItemKindFunction guibg=NONE guifg=#C586C0
	highlight! CmpItemKindMethod guibg=NONE guifg=#C586C0
	" front
	highlight! CmpItemKindKeyword guibg=NONE guifg=#D4D4D4
	highlight! CmpItemKindProperty guibg=NONE guifg=#D4D4D4
	highlight! CmpItemKindUnit guibg=NONE guifg=#D4D4D4
]])

tabnine:setup({
	max_lines = 1000,
	max_num_results = 20,
	sort = true,
	run_on_every_keystroke = true,
	snippet_placeholder = '..',
	ignored_file_types = { -- default is not to ignore
		-- uncomment to ignore in lua:
		-- lua = true
	},
	show_prediction_strength = false,
})

lspkind.init({
	-- DEPRECATED (use mode instead): enables text annotations

	-- default: true
	-- with_text = true,

	-- defines how annotations are shown
	-- options: 'text', 'text_symbol', 'symbol_text', 'symbol'
	mode = 'symbol_text',

	-- default symbol map
	-- can be either 'default' (requires nerd-fonts font) or
	preset = 'default',

	-- override preset symbols
	symbol_map = _cmp_symbol_list,
})

local source_mapping = {
	buffer = "﬘",
	path = "",
	cmdline = "",
	luasnip = "",
	nvim_lsp = "",
	nvim_lua = "",
	spell = "",
	cmp_tabnine = "",
	vsnip = "",
}

local _tabnine_icon_kind = ''

local _comp_item_select_prev_item = cmp.mapping(function(fallback)
	if cmp.visible() then
		cmp.select_prev_item()
	elseif luasnip.jumpable(-1) then
		luasnip.jump(-1)
	else
		fallback()
	end
end, { 'i', 's', 'c' })

local _comp_item_select_next_item = cmp.mapping(function(fallback)
	if cmp.visible() then
		cmp.select_next_item()
	elseif luasnip.jumpable(1) then
		luasnip.jump(1)
	else
		fallback()
	end
end, { 'i', 's', 'c' })

cmp.setup {
	-- preselect = cmp.PreselectMode.None, -- disabled automatically select a particular item
	-- completion = { -- disable auto-completion
	-- 	autocomlete = false,
	-- }
	sources = cmp.config.sources(
	{
		{ name = 'buffer' },
		{ name = 'path' },
		{ name = 'cmdline' },
		{ name = 'luasnip' },
		{ name = 'nvim_lsp' },
		{ name = 'nvim_lua' },
		{ name = 'spell' },
		{ name = 'cmp_tabnine' },
		{ name = 'vsnip' },
	}
	),
	snippet = {
		expand = function(args)
			luasnip.lsp_expand(args.body)
		end,
	},
 	sorting = {
 		priority_weight = 2,
 		comparators = {
				compare.offset,
                compare.exact,
                compare.score,
                compare.recently_used,
                require("cmp-under-comparator").under,
                require("cmp_tabnine.compare"),
                compare.kind,
                compare.sort_text,
                compare.length,
                compare.order
 		}
 	},
	formatting = {
		format = function(entry, vim_item)
			-- vim_item.kind = lspkind.presets.default[vim_item.kind] -- default
			vim_item.kind = lspkind.symbol_map[vim_item.kind]
			local menu = source_mapping[entry.source.name]
			if entry.source.name == 'cmp_tabnine' then
				if entry.completion_item.data ~= nil and entry.completion_item.data.detail ~= nil then
					-- menu = entry.completion_item.data.detail .. ' ' .. menu
					vim_item.kind =  _tabnine_icon_kind .. '  ' .. entry.completion_item.data.detail
				end
			end
			vim_item.menu = menu
			return vim_item
		end,
	},
	mapping = cmp.mapping.preset.insert({
		-- ['<C-d>'] = cmp.mapping.scroll_docs(-4),
		-- ['<C-f>'] = cmp.mapping.scroll_docs(4),
		['<C-Space>'] = cmp.mapping.complete({
			config = {
				sources = {
					{ name = 'vsnip' }
				},
			},
		}),
		['<CR>'] = cmp.mapping.confirm {
			behavior = cmp.ConfirmBehavior.Replace,
			select = true,
		},
		['<Tab>'] = cmp.mapping(function(fallback)
			if cmp.visible() then
				local entry = cmp.get_selected_entry()
				if not entry then
					cmp.select_next_item({
						behavior = cmp.SelectBehavior.Select
					})
				end
				cmp.confirm()
			else
				fallback()
			end
		end, { 'i', 's', 'c' }),
		['<Up>'] = _comp_item_select_prev_item,
		['<S-Tab>'] = _comp_item_select_prev_item,
		['<Down>'] = _comp_item_select_next_item,
		['<C-w>'] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.close()
			end
			fallback()
		end, { 'i', 's', 'c' }),
	}),
	window = {
        completion = cmp.config.window.bordered(),
        documentation = cmp.config.window.bordered(),
	}
}

cmp.setup.cmdline(':', {
	sources = {
		{ name = 'cmdline' },
		{ name = 'path' }
	}
})

cmp.setup.cmdline('/', {
	sources = {
		{ name = 'buffer' }
	}
})

cmp.setup.cmdline('?', {
	sources = {
		{ name = 'buffer' }
	}
})

-- lsp_signature
lsp_signature.setup({
	debug = false, -- set to true to enable debug logging
	log_path = vim.fn.stdpath("cache") .. "/lsp_signature.log", -- log dir when debug is on
	-- default is  ~/.cache/nvim/lsp_signature.log
	verbose = false, -- show debug line number

	bind = true, -- This is mandatory, otherwise border config won't get registered.
				 -- If you want to hook lspsaga or other signature handler, pls set to false
	doc_lines = 10, -- will show two lines of comment/doc(if there are more than two lines in doc, will be truncated);
				 -- set to 0 if you DO NOT want any API comments be shown
				 -- This setting only take effect in insert mode, it does not affect signature help in normal
				 -- mode, 10 by default

	floating_window = true, -- show hint in a floating window, set to false for virtual text only mode

	floating_window_above_cur_line = true, -- try to place the floating above the current line when possible Note:
	-- will set to true when fully tested, set to false will use whichever side has more space
	-- this setting will be helpful if you do not want the PUM and floating win overlap

	floating_window_off_x = 1, -- adjust float windows x position.
	floating_window_off_y = 1, -- adjust float windows y position.


	fix_pos = false,  -- set to true, the floating window will not auto-close until finish all parameters
	hint_enable = true, -- virtual hint enable
	hint_prefix = "💬 ",  -- Panda for parameter "🐼 "
	hint_scheme = "String",
	hi_parameter = "LspSignatureActiveParameter", -- how your parameter will be highlight
	max_height = 12, -- max height of signature floating_window, if content is more than max_height, you can scroll down
					 -- to view the hiding contents
	max_width = 80, -- max_width of signature floating_window, line will be wrapped if exceed max_width
	handler_opts = {
		border = "rounded"   -- double, rounded, single, shadow, none
	},

	always_trigger = true, -- sometime show signature on new line or in middle of parameter can be confusing, set it to false for #58

	auto_close_after = 3, -- autoclose signature float win after x sec, disabled if nil.
	extra_trigger_chars = {}, -- Array of extra characters that will trigger signature completion, e.g., {"(", ","}
	zindex = 200, -- by default it will be on top of all floating windows, set to <= 50 send it to bottom

	padding = '', -- character to pad on left and right of signature can be ' ', or '|'  etc

	transparency = 90, -- disabled by default, allow floating win transparent value 1~100
	shadow_blend = 36, -- if you using shadow as border use this set the opacity
	shadow_guibg = 'Black', -- if you using shadow as border use this set the color e.g. 'Green' or '#121315'
	timer_interval = 150, -- default timer check interval set to lower value if you want to reduce latency
	toggle_key = '<C-j>' -- toggle signature on and off in insert mode,  e.g. toggle_key = '<M-x>'
})

-- fidget
require('fidget').setup {  }


