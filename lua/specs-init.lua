-- Faders:
-- 	linear_fader ▁▂▂▃▃▄▄▅▅▆▆▇▇██

-- 	exp_fader ▁▁▁▁▂▂▂▃▃▃▄▄▅▆▇

-- 	pulse_fader ▁▂▃▄▅▆▇█▇▆▅▄▃▂▁

-- 	empty_fader ▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁

-- Resizers:

-- 	shrink_resizer ░░▒▒▓█████▓▒▒░░

-- 	slide_resizer ████▓▓▓▒▒▒▒░░░░

-- 	empty_resizer ███████████████

require('specs').setup {
    show_jumps  = true,
    min_jump = 30,
    popup = {
        delay_ms = 0, -- delay before popup displays
        inc_ms = 12, -- time increments used for fade/resize effects
        blend = 10, -- starting blend, between 0-100 (fully transparent), see :h winblend
        width = 32,
        winhl = "PMenu",
        fader = require('specs').exp_fader,
        resizer = require('specs').shrink_resizer
    },
    ignore_filetypes = {},
    ignore_buftypes = {
        nofile = true,
    },
}
