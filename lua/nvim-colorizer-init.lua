-- Use the `default_options` as the second parameter, which uses
-- `foreground` for every mode. This is the inverse of the previous
-- setup configuration.
require 'colorizer'.setup({
	-- use default color mode filetype
	-- 'javascript', -- file use default highlighting
    -- '!vim', -- Exclude vim from highlighting.
    '*', -- Highlight all files, but customize some others.

	-- opton color display mode filetype, likes 'html' use 'foreground' mode
	html = { mode = 'foreground' },
	},
	-- default color display mode
	{ mode = 'background' }
)

