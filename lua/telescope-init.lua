
-- local builtin = require('telescope.builtin')
-- local themes = require('telescope.themes')
-- builtin.find_files(themes.get_dropdown())
-- builtin.find_files(themes.get_cursor())
-- builtin.lsp_references(themes.get_cursor())
-- builtin.find_files(themes.get_ivy())

require('telescope').setup{
	defaults = {
		-- Default configuration for telescope goes here:
		-- config_key = value,
		mappings = {
			i = {
				-- map actions.which_key to <C-h> (default: <C-/>)
				-- actions.which_key shows the mappings for your picker,
				-- e.g. git_{create, delete, ...}_branch for the git_branches picker
				["<C-h>"] = "which_key",
				-- ["<leader>ff"] = "<cmd>lua require('telescope.builtin').find_files()<cr>",
			},
			n = {
			}
		},
		sorting_strategy = 'ascending',
		layout_strategy = 'vertical',
		layout_config = {
			vertical = {
				prompt_position = 'top',
			}
		},
	},
	pickers = {
		-- Default configuration for builtin pickers goes here:
		-- picker_name = {
		--   picker_config_key = value,
		--   ...
		-- }
		-- Now the picker_config_key will be applied every time you call this
		-- builtin picker
	},
	extensions = {
		-- Your extension configuration goes here:
		-- extension_name = {
		--   extension_config_key = value,
		-- }
		-- please take a look at the readme of the extension you want to configure
	}
}

Get_telescope_find_file_sty = function ()
	local builtin = require('telescope.builtin')
	-- local themes = require('telescope.themes')

	-- builtin.find_files(themes.get_dropdown())
	builtin.find_files()

end


