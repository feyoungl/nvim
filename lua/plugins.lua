-- >>> this file for init plugins of packer.lua
-- This file can be loaded by calling `lua require('plugins')` from your init.vim

-- ==== obtained system type ====
local BinaryFormat = package.cpath:match("%p[\\|/]?%p(%a+)")
-- if BinaryFormat == "dll" then
--     function os.name()
--         return "Windows"
--     end
-- elseif BinaryFormat == "so" then
--     function os.name()
--         return "Linux"
--     end
-- elseif BinaryFormat == "dylib" then
--     function os.name()
--         return "MacOS"
--     end
-- end
-- ==== end ====


-- cmp-tabnine
-- local _cmp_tabnine_run_shell = './install.sh' -- Linux, Mac
-- local _cmp_tabnine_run_shell = 'powershell ./install.ps1' -- Windows
local _cmp_tabnine_run_shell = function ()
	if BinaryFormat == "dll" then
		vim.notify('Arch: windows', 'Info')
		return 'powershell $env:XDG_DATA_HOME/nvim-data/site/pack/packer/start/cmp-tabnine/install.ps1'
	else
		vim.notify('Arch: linux or mac', 'Info')
		return '$XDG_DATA_HOME/nvim/site/pack/packer/start/cmp-tabnine/install.sh'
	end
end

-- Only required if you have packer configured as `opt`
vim.cmd([[packadd packer.nvim]])

return require('packer').startup(function(use)
	-- Packer can manage itself
	use { 'wbthomason/packer.nvim' }

	-- Is using a standard Neovim install, i.e. built from source or using a
	-- provided appimage.
	use { 'lewis6991/impatient.nvim' }
	use { "nathom/filetype.nvim" }

	use { 'EdenEast/nightfox.nvim' } -- style

	-- Collection of configurations for the built-in LSP client
	use { 'williamboman/nvim-lsp-installer' }
	use { 'neovim/nvim-lspconfig' }
	use { 'onsails/lspkind.nvim' }
	use { 'tami5/lspsaga.nvim' }
	use { 'rcarriga/nvim-notify' }
	use {
		'j-hui/fidget.nvim', -- provide a UI for nvim-lspconfig progress handler
		requires = 'neovim/nvim-lspconfig'
	}

	use {
		'hrsh7th/nvim-cmp', -- Autocompletion plugin
		requires = {
			{ 'hrsh7th/cmp-buffer' },
			{ 'hrsh7th/cmp-path' },
			{ 'hrsh7th/cmp-cmdline' },
			{ 'hrsh7th/cmp-nvim-lsp' }, -- LSP source for nvim-cmp
			{ 'hrsh7th/cmp-nvim-lua' },
			{ 'saadparwaiz1/cmp_luasnip' }, -- Snippets source for nvim-cmp
			{ 'f3fora/cmp-spell' },
			{ 'L3MON4D3/LuaSnip' }, -- Snippets plugin
			{ 'lukas-reineke/cmp-under-comparator' },
			{
				'tzachar/cmp-tabnine', -- completion
				run = _cmp_tabnine_run_shell,
				requires = 'hrsh7th/nvim-cmp',
				opt = false,
			}
		}
	}

	use { 'ahmedkhalf/project.nvim' } -- project

	use {
		'kyazdani42/nvim-tree.lua',
		requires = {
			'kyazdani42/nvim-web-devicons', -- optional, for file icon
		},
		tag = 'nightly' -- optional, updated every week. (see issue #1193)
	}

	-- using packer.nvim
	use {'akinsho/bufferline.nvim', tag = "v2.*", requires = 'kyazdani42/nvim-web-devicons'}

	use {
		'nvim-lualine/lualine.nvim',
		requires = { 'kyazdani42/nvim-web-devicons' }
	}

	-- Plugins can have post-install/update hooks
	-- use {'iamcco/markdown-preview.nvim', run = 'cd app && yarn install', cmd = 'MarkdownPreview'}

	-- Post-install/update hook with neovim command
	use { 'nvim-treesitter/nvim-treesitter',  run = ':TSUpdate' }
	use { 'nvim-treesitter/nvim-treesitter-context' }
	use {
		'p00f/nvim-ts-rainbow',
		requires = { 'nvim-treesitter/nvim-treesitter' }
	}

	use {
		'nvim-telescope/telescope.nvim',
		requires = { 'nvim-lua/plenary.nvim' }
	}

	use {
		'lewis6991/gitsigns.nvim',
		tag = 'release' -- To use the latest release
	}

	use { 'jiangmiao/auto-pairs' }
	use { 'numToStr/Comment.nvim' }

	use {
		'folke/todo-comments.nvim',
		requires = 'nvim-lua/plenary.nvim'
	}

	use { 'norcalli/nvim-colorizer.lua' }
	use { 'phaazon/hop.nvim', }

	use { 'ray-x/lsp_signature.nvim' }
	use { 'yamatsum/nvim-cursorline' }
	use { 'junegunn/vim-easy-align' }
	use { 'edluffy/specs.nvim' }
	use { 'mg979/vim-visual-multi' }
	use { 'tpope/vim-surround' }
	use { 'gcmt/wildfire.vim' } -- want to select block code
	use { 'simrat39/symbols-outline.nvim' }

















end)

