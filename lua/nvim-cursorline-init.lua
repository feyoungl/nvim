require('nvim-cursorline').setup {
  cursorline = {
    enable = true,
    timeout = 1000,
    number = false, -- false: line number always highlight
  },
  cursorword = {
    enable = true,
    min_length = 3,
    hl = { underline = true },
  }
}
