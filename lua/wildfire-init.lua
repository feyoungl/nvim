-- This file is wildfire to select code block in visual mode

vim.cmd([[
	" use '*' to mean 'all other filetypes'
	" in this example, html and xml share the same text objects
	let g:wildfire_objects = {
		\ "*" : ["i'", 'i"', "i)", "i]", "i}"],
		\ "html,xml" : ["at", "it"],
	\ }
]])
