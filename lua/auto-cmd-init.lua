-- this file for auto cmd init.

vim.cmd([[

" fix first time can not fold by telescope
augroup _fold_bug_solution 	" https://github.com/nvim-telescope/telescope.nvim/issues/559
	autocmd!
	autocmd BufRead * autocmd BufWinEnter * ++once normal! zx
augroup end


" lspsaga
augroup _lspsaga_filetypes
  autocmd!
  autocmd FileType LspsagaHover nnoremap <buffer><nowait><silent> <Esc> <cmd>close!<cr>
augroup end

]])



