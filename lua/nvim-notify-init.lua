-- test tip box

local notify = require('notify')

notify.setup({
	background_colour = nil, -- 'Normal'
	fps = 60,
	icons = {
		DEBUG = "",
		ERROR = "",
		INFO = "",
		TRACE = "✎",
		WARN = ""
	},
	level = "info",
	minimum_width = 50,
	render = "default", -- 'default', 'minimal'
	stages = "fade_in_slide_out", -- 'fade_in_slide_out', 'fade', ''slide', 'static'
	timeout = 3000
})

vim.notify = notify
