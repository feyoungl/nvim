
local hop = require('hop')
hop.setup {
	keys = 'etovxqpdygfblzhckisuran',
	jump_on_sole_occurrence = false,
	multi_windows = true, -- can jump different windows.
}

