-- this file is custom key-remapping.
--
-- example:
-- 	vim.api.nvim_set_keymap('n', '<leader><leader>', ':set hlsearch!<cr>', { noremap = true, silent = true })
-- 	:nnoremap <silent> <leader><leader> :set hlsearch<cr>
--
-- 	vim.api.nvim_buf_set_keymap(0, '', 'cc', 'line(".") == 1 ? "cc" : "ggcc"', { noremap = true, expr = true })
-- 	:noremap <buffer> <expr> cc line('.') == 1 ? 'cc' : 'ggcc'
--

local opts			= { noremap = true, silent = false }
local opts_silent	= { noremap = true, silent = true }

-- 'gitsigns-nvim-init.lua' keymap

-- insert mode 'jj' to normal mode
vim.api.nvim_set_keymap('i', 'jj', '<esc>', opts_silent)
vim.api.nvim_set_keymap('i', 'kk', '<esc>', opts_silent)
vim.api.nvim_set_keymap('i', 'hh', '<esc>', opts_silent)

-- lsp-installing
vim.api.nvim_set_keymap('n', '<leader>??', ':PackerSync<cr>', opts_silent)

-- window
-- ctrl hjkl to change window
vim.api.nvim_set_keymap('n', '<C-j>', '<C-w>j', opts_silent)
vim.api.nvim_set_keymap('n', '<C-k>', '<C-w>k', opts_silent)
vim.api.nvim_set_keymap('n', '<C-h>', '<C-w>h', opts_silent)
vim.api.nvim_set_keymap('n', '<C-l>', '<C-w>l', opts_silent)

vim.api.nvim_set_keymap('n', '<S-j>', '<C-w>+', opts_silent)
vim.api.nvim_set_keymap('n', '<S-k>', '<C-w>-', opts_silent)
vim.api.nvim_set_keymap('n', '<S-h>', '<C-w>>', opts_silent)
vim.api.nvim_set_keymap('n', '<S-l>', '<C-w><', opts_silent)

-- buffferline
vim.api.nvim_set_keymap('n', 'gl', '<Cmd>BufferLinePick<cr>', opts_silent)

vim.api.nvim_set_keymap('n', '<leader>n', ':BufferLineCyclePrev <cr>', opts_silent)
vim.api.nvim_set_keymap('n', '<leader>m', ':BufferLineCycleNext <cr>', opts_silent)

vim.api.nvim_set_keymap('n', '<leader>1', '<Cmd>BufferLineGoToBuffer 1<cr>', opts_silent)
vim.api.nvim_set_keymap('n', '<leader>2', '<Cmd>BufferLineGoToBuffer 2<cr>', opts_silent)
vim.api.nvim_set_keymap('n', '<leader>3', '<Cmd>BufferLineGoToBuffer 3<cr>', opts_silent)
vim.api.nvim_set_keymap('n', '<leader>4', '<Cmd>BufferLineGoToBuffer 4<cr>', opts_silent)
vim.api.nvim_set_keymap('n', '<leader>5', '<Cmd>BufferLineGoToBuffer 5<cr>', opts_silent)
vim.api.nvim_set_keymap('n', '<leader>6', '<Cmd>BufferLineGoToBuffer 6<cr>', opts_silent)
vim.api.nvim_set_keymap('n', '<leader>7', '<Cmd>BufferLineGoToBuffer 7<cr>', opts_silent)
vim.api.nvim_set_keymap('n', '<leader>8', '<Cmd>BufferLineGoToBuffer 8<cr>', opts_silent)
vim.api.nvim_set_keymap('n', '<leader>9', '<Cmd>BufferLineGoToBuffer 9<cr>', opts_silent)
vim.api.nvim_set_keymap('n', '<C-w>', '<Cmd>BufferLinePickClose<cr>', opts_silent)
vim.api.nvim_set_keymap('n', '<leader><leader>t', '<Cmd>BufferLineTogglePin<cr>', opts_silent)

-- telescope
-- vim.api.nvim_set_keymap('n', '<leader>ff', '<cmd>lua require(\'telescope.builtin\').find_files()<cr>', opts_silent)
vim.api.nvim_set_keymap('n', '<leader>ff', '<cmd>lua Get_telescope_find_file_sty()<cr>', opts_silent)
vim.api.nvim_set_keymap('n', '<leader>fg', '<cmd>lua require(\'telescope.builtin\').live_grep()<cr>', opts_silent)
vim.api.nvim_set_keymap('n', '<leader>fl', '<cmd>lua require(\'telescope.builtin\').buffers()<cr>', opts_silent)
vim.api.nvim_set_keymap('n', '<leader>fh', '<cmd>lua require(\'telescope.builtin\').help_tags()<cr>', opts_silent)
vim.api.nvim_set_keymap('n', '<leader>/', '<cmd>lua require(\'telescope.builtin\').current_buffer_fuzzy_find()<cr>', opts_silent)
vim.api.nvim_set_keymap('n', '<leader>gs', '<cmd>lua require(\'telescope.builtin\').git_status()<cr>', opts_silent)
vim.api.nvim_set_keymap('n', '<leader>gt', '<cmd>lua require(\'telescope.builtin\').git_stash()<cr>', opts_silent)

-- nvimtree
vim.api.nvim_set_keymap('n', '<leader>tt', ':NvimTreeToggle<cr>', opts_silent)

-- pop.nvim
-- place this in one of your configuration file(s)
vim.api.nvim_set_keymap('n', '<leader><leader>w', ':HopWord<cr>', opts_silent)
vim.api.nvim_set_keymap('n', '<leader><leader>l', ':HopLine<cr>', opts_silent)
vim.api.nvim_set_keymap('n', '<leader><leader>s', ':HopChar1<cr>', opts_silent)
vim.api.nvim_set_keymap('n', '<leader><leader>p', ':HopPattern<cr>', opts_silent)

-- nvim-lspconfig
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
-- vim.api.nvim_set_keymap('n', '<leader>e', '<cmd>lua vim.diagnostic.open_float()<cr>', opts_silent) -- same as Lspsaga show_line_diagnostics
-- vim.api.nvim_set_keymap('n', '[d', '<cmd>lua vim.diagnostic.goto_prev()<cr>', opts_silent) -- same as Lspsaga diagnostic_jump_prev
-- vim.api.nvim_set_keymap('n', ']d', '<cmd>lua vim.diagnostic.goto_next()<cr>', opts_silent) -- same as Lspsaga diagnostic_jump_next
vim.api.nvim_set_keymap('n', '<leader>q', '<cmd>lua vim.diagnostic.setloclist()<cr>', opts_silent)


--- In lsp attach function
-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
Lsp_on_attach = function(client, bufnr)
	local _ = client

	-- Enable completion triggered by <c-x><c-o>
	vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

	-- Mappings.
	-- See `:help vim.lsp.*` for documentation on any of the below functions
	vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<cr>', opts_silent)
	vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gd', '<cmd>lua vim.lsp.buf.definition()<cr>', opts_silent)
	vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<cr>', opts_silent)
	vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>D', '<cmd>lua vim.lsp.buf.type_definition()<cr>', opts_silent)
	-- workspace
	vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<cr>', opts)
	vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<cr>', opts_silent)
	vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<cr>', opts_silent)


	vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader><leader>fm', '<cmd>lua vim.lsp.buf.formatting()<cr>', opts_silent)

	-- noused
	-- vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>he', '<cmd>lua vim.lsp.buf.signature_help()<cr>', opts)

	-- maybe change to key-remapping use 'Lspsaga' plugin.
	-- vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>rn', '<cmd>lua vim.lsp.buf.rename()<cr>', opts)
	-- vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gh', '<cmd>lua vim.lsp.buf.hover()<cr>', opts)
	-- vim.api.nvim_buf_snt_keymap(bufnr, 'n', 'gr', '<cmd>lua vim.lsp.buf.references()<cr>', opts)
	-- vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>a', '<cmd>lua vim.lsp.buf.code_action()<cr>', opts)
end


-- Lspsaga
-- maybe change to key-remapping use 'lspconfig' default.
vim.api.nvim_set_keymap("n", "<leader>rn", "<cmd>Lspsaga rename<cr>", opts_silent)
vim.api.nvim_set_keymap("n", "<leader>k",  "<cmd>Lspsaga hover_doc<cr>", opts_silent)
vim.api.nvim_set_keymap("n", "<leader>a", "<cmd>Lspsaga code_action<cr>", opts_silent)

-- using key-remapping
vim.api.nvim_set_keymap("x", "ge", ":<c-u>Lspsaga range_code_action<cr>", opts_silent)
vim.api.nvim_set_keymap("n", "gh", "<cmd>lua require'lspsaga.provider'.preview_definition()<cr>", opts_silent)
vim.api.nvim_set_keymap("n", "gr", "<cmd>lua require'lspsaga.provider'.lsp_finder()<cr>", opts_silent) -- references
vim.api.nvim_set_keymap("n", "go", "<cmd>Lspsaga show_line_diagnostics<cr>", opts_silent)
vim.api.nvim_set_keymap("n", "gj", "<cmd>Lspsaga diagnostic_jump_next<cr>", opts_silent)
vim.api.nvim_set_keymap("n", "gk", "<cmd>Lspsaga diagnostic_jump_prev<cr>", opts_silent)
-- 悬浮窗口上翻页，由 Lspsaga 提供
vim.api.nvim_set_keymap("n", "<C-u>", "<cmd>lua require('lspsaga.action').smart_scroll_with_saga(-4, '<c-u>')<cr>", opts_silent)
-- 悬浮窗口下翻页，由 Lspsaga 提供
vim.api.nvim_set_keymap("n", "<C-d>", "<cmd>lua require('lspsaga.action').smart_scroll_with_saga(4, '<c-d>')<cr>", opts_silent)

-- terminal
vim.api.nvim_set_keymap("n", "<C-p>", "<cmd>lua require('lspsaga.floaterm').open_float_terminal()<cr>", opts_silent)
vim.api.nvim_set_keymap("t", "<C-p>", "<C-\\><C-n><cmd>lua require('lspsaga.floaterm').close_float_terminal()<cr>", opts_silent)

-- vim-easy-align
vim.api.nvim_set_keymap("x", "ga", "<Plug>(EasyAlign)",  opts)
vim.api.nvim_set_keymap("n", "ga", "<Plug>(EasyAlign)", opts)

-- specs cursor
-- Press <C-b> to call specs!
-- vim.api.nvim_set_keymap('n', '<C-b>', ':lua require("specs").show_specs()', opts_silent)

-- You can even bind it to search jumping and more, example:
vim.api.nvim_set_keymap('n', 'n', 'n:lua require("specs").show_specs()<cr>', opts_silent)
vim.api.nvim_set_keymap('n', 'N', 'N:lua require("specs").show_specs()<cr>', opts_silent)
vim.api.nvim_set_keymap('n', '{', '{:lua require("specs").show_specs()<cr>', opts_silent)
vim.api.nvim_set_keymap('n', '}', '}:lua require("specs").show_specs()<cr>', opts_silent)
vim.api.nvim_set_keymap('n', '[c', '[c:lua require("specs").show_specs()<cr>', opts_silent)
vim.api.nvim_set_keymap('n', ']c', ']c:lua require("specs").show_specs()<cr>', opts_silent)
vim.api.nvim_set_keymap('n', '<C-d>', '<C-d>:lua require("specs").show_specs()<cr>', opts_silent)
vim.api.nvim_set_keymap('n', '<C-u>', '<C-u>:lua require("specs").show_specs()<cr>', opts_silent)

-- symbols-outline
vim.api.nvim_set_keymap('n', '<leader><leader>o', ':SymbolsOutline<cr>', opts_silent)







