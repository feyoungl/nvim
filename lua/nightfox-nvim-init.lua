
-- style palettes
-- │  Key  │             Description             │
-- │black  │Shade Color (Base, Bright, Dim)      │
-- │red    │Shade Color (Base, Bright, Dim)      │
-- │green  │Shade Color (Base, Bright, Dim)      │
-- │yellow │Shade Color (Base, Bright, Dim)      │
-- │blue   │Shade Color (Base, Bright, Dim)      │
-- │magenta│Shade Color (Base, Bright, Dim)      │
-- │cyan   │Shade Color (Base, Bright, Dim)      │
-- │white  │Shade Color (Base, Bright, Dim)      │
-- │orange │Shade Color (Base, Bright, Dim)      │
-- │pink   │Shade Color (Base, Bright, Dim)      │
-- │comment│Comment color                        │
-- │bg0    │Darker bg (status line and float)    │
-- │bg1    │Default bg                           │
-- │bg2    │Lighter bg (colorcolumn folds)       │
-- │bg3    │Lighter bg (cursor line)             │
-- │bg4    │Lighter bg (Conceal, boarder fg)     │
-- │fg0    │Lighter fg                           │
-- │fg1    │Default fg (title content fg fey)    │
-- │fg2    │Darker fg (status line)              │
-- │fg3    │Darker fg (line numbers, fold colums)│
-- │sel0   │Popup bg, visual selection bg        │
-- │sel1   │Popup sel bg, search bg              │

local _palettes = {
	nightfox = {
		bg0 = '#222222', -- status line total change color
		bg1 = '#1d1d1d'
	},
	-- other style
}

require('nightfox').setup({
	palettes = _palettes,
	options = {
		transparent = false,    -- Disable setting background
		terminal_colors = true, -- Set terminal colors (vim.g.terminal_color_*) used in `:terminal`
		dim_inactive = true,   -- Non focused panes set to alternative background
		styles = {
			comments = '', -- 'italic'
			keywords = 'bold',
			types = 'bold',
		},
		inverse = {
			-- Inverse highlight for different types
			match_paren = true,
			visual = true,
			search = true,
		},
		module = {
			-- other plugins @see :h nightfox
		},
	},
})


vim.cmd([[
	colorscheme nightfox
]])


