# This file is alias in Windows
#
# local Path:
# 	$HOME/Documents/WindowsPowerShell/Microsoft.PowerShell_profile.ps1
#
function gmake {make}
function vi {nvim}
function vim {nvim}
function lg {lazygit}

# Import the Chocolatey Profile that contains the necessary code to enable
# tab-completions to function for `choco`.
# Be aware that if you are missing these lines from your profile, tab completion
# for `choco` will not function.
# See https://ch0.co/tab-completion for details.
$ChocolateyProfile = "$env:ChocolateyInstall\helpers\chocolateyProfile.psm1"
if (Test-Path($ChocolateyProfile)) {
  Import-Module "$ChocolateyProfile"
}

# powershell
# Install-Module PSReadLine -RequiredVersion 2.1.0
# Import-Module PSReadLine
# Set-PSReadLineOption -PredictionSource History
# Test-path $profile
# notepad $profile

# Shows navigable menu of all options when hitting Tab
Set-PSReadlineKeyHandler -Key Tab -Function MenuComplete

# Autocompletion for arrow keys
Set-PSReadlineKeyHandler -Key UpArrow -Function HistorySearchBackward
Set-PSReadlineKeyHandler -Key DownArrow -Function HistorySearchForward

# auto suggestions
Import-Module PSReadLine
Set-PSReadLineOption -PredictionSource History
