-- My customized Neovim work environment.
-- this is my code.
--
-- The config file is located at:
--   Unix        ~/.config/nvim/init.vim (or init.lua)
--   Windows     ~/AppData/Local/nvim/init.vim (or init.lua)
--
-- if $XDG_CONFIG_HOME is defined:
--           $XDG_CONFIG_HOME/nvim/init.vim (or init.lua)
--
-- as same: $XDG_DATA_HOME
--
--
-- :checkhealth
-- :PlugStatus
-- :PlugInstall
--
-- tree-sitter github down window
--
-- prel[option]:
--   cpan install Neovim::Ext moudle
--
-- lsp request:
--   1. node.js
--   		remark
--   2. npm or yarn
--   3. luarocks
--   4. chocolatey -- important
--   		neovim
--   5. python3
--   		pyvim
--
-- telescope request:
--   `brew install ripgrep` or `choco install ripgrep`
--   `brew install fd` or `sudo apt install fd-find` or `choco install fd`
--
-- windows PATH:
-- user PATH:
--   %ChocolateyToolsLocation% = 'D:\envs\chocolatey\tools'
--   %GOPATH% = 'C:\Users\feiya\go'
-- sys PATH:
--   %CHOCO_TOOLS_HOME% = 'D:\envs\chocolatey\tools'
--   %ChocolateyInstall% = 'D:\envs\chocolatey\chocolatey'
--   %NEOVIM% = '%CHOCO_TOOLS_HOME%\neovim\nvim-win64'
--   %XDG_CONFIG_HOME% = 'D:\Neonvim'
--   %XDG_DATA_HOME% = 'D:\Neonvim'

local options = {
	fileencodings  = 'utf-8,gb2312,gb18030,gbk,ucs-bom,cp936',
	termencoding   = 'utf-8',
	encoding       = 'utf-8',
	tabstop        = 4,
	shiftwidth     = 4,
	number         = true,
	relativenumber = true,
	termguicolors  = true,
	smartindent    = true,
	autoindent     = true,
	cursorline     = true,
	ignorecase     = true,
	smartcase      = true,
	hlsearch       = true,
	incsearch      = true, -- searching while inputing character
	splitbelow     = true,
	splitright     = true,
	history        = 199,
	jumpoptions    = 'stack',
	autoread       = true,
	signcolumn     = "yes:2",
	cmdheight      = 2,
	clipboard      = 'unnamed',
	foldmethod     = 'expr', -- fold with nvim-treesitter such as 'indent', 'syntax'
	foldexpr       = 'nvim_treesitter#foldexpr()',
	foldenable     = false, -- no fold to be applied when open a file
	foldlevel      = 99, -- if not set this, fold will be everywhere
	wildmenu       = true,
	wildmode       = longest,

	-- ambiwidth   = 'double',	-- WARNING lcd will have chars can not clean.
	-- backspace   = 2,			-- can not backclean chars
}

vim.g.nobackup = true
vim.g.ale_sign_column_always = true

for k, v in pairs(options) do
	vim.opt[k] = v
end

vim.cmd([[
	filetype on
	syntax enable
	syntax on
]])

-- ==== ssh romote clip board ====
vim.cmd([[
if executable('clipboard-provider')
    let g:clipboard = {
        \ 'name': 'myClipboard',
        \     'copy': {
        \         '+': 'clipboard-provider copy',
        \         '*': 'env COPY_PROVIDERS=tmux clipboard-provider copy',
        \     },
        \     'paste': {
        \         '+': 'clipboard-provider paste',
        \         '*': 'env COPY_PROVIDERS=tmux clipboard-provider paste',
        \     },
        \ }
endif
]])


-- leader key map
vim.g.mapleader = ' '


-- autocmd and keyremap
require('auto-cmd-init')
require('key-mapping-custom-init')

-- export 'lua/plugins.lua' to nvim runtimes.
require('plugins')
require('nightfox-nvim-init')
require('filetype-init')

require('nvim-notify-init')
require('lspsaga-init')
require('lsp-cmp-init')
require('nvim-tree-init')
require('nvim-treesitter-init')
require('nvim-treesitter-content-init')

require('telescope-init')
require('bufferline-init')
require('lualine-init')
require('gitsigns-nvim-init')
require('comment-init')
require('nvim-colorizer-init')
require('hop-nvim-init')
require('nvim-cursorline-init')
require('vim-easy-align-init')
require('specs-init')
require('symbols-outline-init')

